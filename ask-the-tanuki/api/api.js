const bodyParser = require('body-parser')
const app = require('express')()
const axios = require('axios')

app.use(bodyParser.json())
app.all('/ask', async (req, res) => {
  console.log('R : ' + req.query.q)

  if (req.query.q) {
    try {
      const response = await axios.get(
        'http://127.0.0.1:5000/ask?q=' +
          req.query.q +
          '&source=' +
          req.query.source
      )
      console.log(response.data)
      res.json({
        msg: response.data.msg,
        sources: response.data.sources,
      })
    } catch (error) {
      console.error(error)
      res.json(error)
    }
  }
})

module.exports = app
